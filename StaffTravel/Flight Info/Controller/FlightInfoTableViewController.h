//
//  FlightInfoTableViewController.h
//  StaffTravel
//
//  Created by Mohammed Alquzi on 6/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "FlightInfo.h"
#import "RPFloatingPlaceholderTextField.h"
#import "RPFloatingPlaceholderTextView.h"
#import "FlightDetailsTableViewController.h"
#import "SVProgressHUD.h"
#import "ServicingBL.h"
#import "FlightInfoBL.h"
#import "Utility.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"

//#import "BookingItinerary.h"
//#import "Booking.h"

#define NUMBERS_ONLY @"1234567890"
#define FLIGHTNUMBER_LIMIT 4
#define FLIGHT_INFO_INPUT @"Flight Info Input"

@interface FlightInfoTableViewController : UITableViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,UIWebViewDelegate>
{
    FlightInfoDetail* flightInfo;
    NSString* flightDate;
    IBOutlet UIWebView *webView; //to be usesd reachability
}
- (IBAction)departureDateSwitchbtn:(id)sender;
//@property(strong, nonatomic) IBOutlet UIWebView *webView;
@property(strong, nonatomic) UIWebView *webView;

@property (strong, nonatomic) IBOutlet UILabel *todayYesterdayLabel;
//@property (strong, nonatomic) IBOutlet UITextField *flightNumberLabel;
//@property (strong, nonatomic) IBOutlet UITextField *carrierLabel;
//- (IBAction)displayFlightInfo:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *departureDateSwitchOutlet;
//@property (strong, nonatomic) IBOutlet UITableViewCell *flightNoCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *carrierCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *departureDateCell;
//@property (strong, nonatomic) IBOutlet UITableViewCell *displayButtonCell;

@property (strong, nonatomic) IBOutlet UITableViewCell *flightNumberCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *flightInfoDisplayCell;

@property (strong, nonatomic) IBOutlet RPFloatingPlaceholderTextField *flightNumberTextField;

@property (strong, nonatomic) IBOutlet RPFloatingPlaceholderTextField *carrierTextField;
@property (strong, nonatomic) NSString  *flightNumberTitle;

@property (strong, nonatomic) IBOutlet UIButton *displayFlightInfoOutletButton;

-(void)hudTapped:(NSNotification *)notification;


@end
