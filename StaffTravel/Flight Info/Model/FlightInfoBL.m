//
//  FlightInfoBL.m
//  StaffTravel
//
//  Created by Mohammed Alquzi on 7/7/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "FlightInfoBL.h"

@implementation FlightInfoBL

- (FlightInfoDetail*) retrieveBookingFromJsonOfString:(NSString*)jsonString
{
    NSError *error;
    NSString *jsonModified = [jsonString stringByReplacingOccurrencesOfString:@"<html><head></head><body>" withString:@""];
    jsonModified = [jsonModified stringByReplacingOccurrencesOfString:@"</body></html>" withString:@""];
    NSData *jsonData = [jsonModified dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary* json =[NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"JSON:%@", jsonModified);
    
    
    //JSON
    NSDictionary *dicFlying  =[json objectForKey:@"message"]; //----1
    
    FlightInfoDetail *flightInfoDetail = [[FlightInfoDetail alloc]init];
    flightInfoDetail.messageStatus = [dicFlying objectForKey:@"status"];
    flightInfoDetail.message = [dicFlying objectForKey:@"msg"];
    
    NSLog(@"%@ // s = Successful",[dicFlying objectForKey:@"status"]);
    NSLog(@"status form BL:%@",flightInfoDetail.messageStatus);
    NSLog(@"flightInfoDetail.messageStatus:%@",flightInfoDetail.messageStatus );
  
    NSDictionary *flight_infoDic = [dicFlying objectForKey:@"flight_info"]; //----2
    NSArray* itineraryDic = [flight_infoDic objectForKey:@"itinerary"]; //---3
    NSMutableArray* itineraries = [[NSMutableArray alloc]init]; //---5
    NSDictionary *itnDic = [[NSDictionary alloc]init];

    //Input data
    NSLog(@"%@",[flight_infoDic objectForKey:@"carrier"]);
    NSLog(@"%@",[flight_infoDic objectForKey:@"flight_no"]);
    NSLog(@"%@",[flight_infoDic objectForKey:@"dep_date"]);
    
    //-----------------Loop for Flight Information
    for (int i = 0; i<itineraryDic.count; i++) {
        itnDic = ((NSDictionary*)[itineraryDic objectAtIndex:i]);
        FlightInfoDetail *itn = [[FlightInfoDetail alloc]init];
        
        [itn setDeparture:[itnDic valueForKey:@"departure"]];           //1
        NSLog(@"departure;%@", itn.departure);
        [itn setDepartureTime:[itnDic valueForKey:@"departure_time"]];  //2
        NSLog(@"%@", [itnDic valueForKey:@"departure_time"]);
        [itn setArrivalCity:[itnDic valueForKey:@"arrival_city"]];      //3
        NSLog(@"%@", [itnDic valueForKey:@"arrival_city"]);
        [itn setArrivalTime:[itnDic valueForKey:@"arrival_time"]];      //4
        NSLog(@"%@", [itnDic valueForKey:@"arrival_time"]);
        [itn setDuration:[itnDic valueForKey:@"duration"]];             //5
        NSLog(@"%@", [itnDic valueForKey:@"duration"]);
        [itn setAirCraft:[itnDic valueForKey:@"aircraft"]];             //6
        NSLog(@"%@", [itnDic valueForKey:@"aircraft"]);
        [itn setDepartureDate:[flight_infoDic objectForKey:@"dep_date"]];
        

        itn.messageStatus = flightInfoDetail.messageStatus;
        itn.message = flightInfoDetail.message;
        
        NSArray *actualFlightInfo = [[NSArray alloc]init];
        actualFlightInfo = [flight_infoDic objectForKey:@"actual_flight_info"];
        
        //----------------Loop for Extra Flight Information
        for (int i = 0; i<actualFlightInfo.count; i++) {
            NSLog(@"Actual Info:%@",[actualFlightInfo objectAtIndex:i]);
        }

        //itn.actualFlightInfo = actualFlightInfo;
        [itineraries addObject:itn];
    }
    
    
    flightInfoDetail = [itineraries objectAtIndex:0];
   
    return flightInfoDetail;
}


@end
