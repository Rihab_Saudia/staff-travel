//
//  WebCheckInViewController.h
//  StaffTravel
//
//  Created by Ali Mansho on 8/27/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import "Utility.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"


#define CHECK_IN @"Check In"

@interface WebCheckInViewController : UIViewController <UIWebViewDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *checkInAmadeus;

@end
