//
//  WebCheckInViewController.m
//  StaffTravel
//
//  Created by Ali Mansho on 8/27/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "WebCheckInViewController.h"

@interface WebCheckInViewController ()

@end

@implementation WebCheckInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:CHECK_IN];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Page title
    self.title = @"Check-In";
    
    //2 to show Navigation Bar
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    if([Utility isInternetReachable])
        
    {
    self.checkInAmadeus.delegate = self; 
    NSURL *webCheckInURL = [NSURL URLWithString:@"https://checkin.si.amadeus.net/1ASIHSSCMCISV/sscasv/checkin?ln=en"];
    
    NSURLRequest *webCheckInReq = [NSURLRequest requestWithURL:webCheckInURL];
    [self.checkInAmadeus loadRequest:webCheckInReq];
    [self.view addSubview:self.checkInAmadeus];
    
    //NSLog(@"Mansho - Just started loading... webViewDidStartLoad");
    
    self.checkInAmadeus =[[UIWebView alloc]initWithFrame:self.view.frame];
    
        //[SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hudTapped:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
        [SVProgressHUD showWithStatus:@"Loading...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated    // Called when the view is about to made visible. Default does nothing
{
    NSLog(@"Mansho - ... viewWillAppear");
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
  NSLog(@"webViewDidStartLoad");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
     NSLog(@"webViewDidFinishLoad");
    [SVProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
  NSLog(@"didFailLoadWithError");
    [SVProgressHUD dismiss];
}

//This method is to cancel the laoding when a user taps on it by Ali Mansho (A.M.)
- (void)hudTapped:(NSNotification *)notification
{
    [self.checkInAmadeus stopLoading];
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
}


@end
