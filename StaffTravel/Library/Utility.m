//
//  Utility.m
//  StaffTravel
//
//  Created by Mohammad Bahadur on 8/27/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "Utility.h"

@implementation Utility


//The following method is to test connectivity / reachability in the device...
+ (BOOL)isInternetReachable
{
    Reachability * reachability;
    NetworkStatus internetStatus;
    BOOL isReachable = NO;
    reachability = [Reachability reachabilityForInternetConnection];
    internetStatus = [reachability currentReachabilityStatus];
    
    if(internetStatus == NotReachable)
    {
        UIAlertView *errorView;
        
        errorView = [[UIAlertView alloc]
                     initWithTitle: @"Cannot Connect"
                     message: @"You must connect to a Wi-Fi or cellular data network."                     delegate: self
                     cancelButtonTitle: @"Close" otherButtonTitles: nil];
        
        [errorView show];
        isReachable = NO;
    }
    else
    {
        isReachable = YES;
    }
    
    return isReachable;
}

//The following method is to delete the cookies from the app by Ali Mansho.
+ (void) deleteCookies
{
    //Delete the cookies... By A.M.
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    //******************************* A.M.
}

@end
