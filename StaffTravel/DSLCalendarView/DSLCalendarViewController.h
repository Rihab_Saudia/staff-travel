//
//  DSLCalendarViewController.h
//  SaudiaStaffTravel
//
//  Created by Ali Mansho on 4/7/14.
//  Copyright (c) 2014 Ali Mansho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSLCalendarViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *DepartureDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *ReturnDateLabel;

@end
