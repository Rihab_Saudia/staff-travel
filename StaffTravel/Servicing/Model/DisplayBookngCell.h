//
//  DisplayBookngCell.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 4/27/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPFloatingPlaceholderTextField.h"
#import "RPFloatingPlaceholderTextView.h"

@interface DisplayBookngCell : UITableViewCell
- (IBAction)displayBooking:(id)sender;

@end
