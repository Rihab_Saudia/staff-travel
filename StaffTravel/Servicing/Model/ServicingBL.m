//
//  ServicingBL.m
//  StaffTravel
//
//  Created by Mohammad Bahadur on 6/29/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "ServicingBL.h"

@implementation ServicingBL

- (Booking*) retrieveBookingFromJsonOfString:(NSString*)jsonString
{
    Booking *booking = [[Booking alloc]init];
    NSError *error;
    NSString *jsonModified = [jsonString stringByReplacingOccurrencesOfString:@"<html><head></head><body>" withString:@""];
    jsonModified = [jsonModified stringByReplacingOccurrencesOfString:@"</body></html>" withString:@""];
    NSData *jsonData = [jsonModified dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary* json =[NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    //NSDictionary* json =[NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"JSON:%@", jsonModified);
    
    NSDictionary* dicBooking = [json objectForKey:@"message"];
    NSDictionary* pnrDic = [dicBooking objectForKey:@"pnr"];
    NSArray* names = [pnrDic objectForKey:@"names"];
    NSArray* tickets = [pnrDic objectForKey:@"tickets"];
    NSArray* itinerariesDic = [pnrDic objectForKey:@"itineraries"];
    NSMutableArray* itineraries = [[NSMutableArray alloc]init];
    NSDictionary *itnDic = [[NSDictionary alloc]init];
    BookingItinerary *itn;
    
    for (int i = 0; i<itinerariesDic.count; i++) {
        itnDic = ((NSDictionary*)[itinerariesDic objectAtIndex:i]);
        itn = [[BookingItinerary alloc]init];
        
        [itn setCarrier:[itnDic valueForKey:@"carrier"]];
        [itn setRefNumber:[itnDic valueForKey:@"refnum"]];
        [itn setFlightNo:[itnDic valueForKey:@"flightno"]];
        [itn setCabinClass:[itnDic valueForKey:@"class"]];
        [itn setDay:[itnDic valueForKey:@"day"]];
        [itn setDepDate:[itnDic valueForKey:@"depdate"]];
        [itn setOrigin:[itnDic valueForKey:@"origin"]];
        [itn setDest:[itnDic valueForKey:@"dest"]];
        [itn setDepartureTime:[itnDic valueForKey:@"departure"]];
        [itn setArrivalTime:[itnDic valueForKey:@"arrival"]];
        [itn setStatus:[itnDic valueForKey:@"status"]];
        [itineraries addObject:itn];
    }
    itn = nil;
    
    NSLog(@"message status:%@", [dicBooking objectForKey:@"status"]);
    
    
    booking = [[Booking alloc]init];
    [booking setItineraries:itineraries];
    [booking setAlphanumeric:[pnrDic objectForKey:@"alphanumeric"]];
    [booking setNumeric:[pnrDic objectForKey:@"numeric"]];
    [booking setNames:names];
    [booking setTickets:tickets];
    [booking setStatus:[dicBooking valueForKey:@"status"]];
    [booking setMessage:[dicBooking valueForKey:@"msg"]];
    [booking setTimelimit:[pnrDic valueForKey:@"tlt"]];

    return booking;
}

- (MessageResponse*) getMessageResponseFromJsonOfString:(NSString*)jsonString
{
    MessageResponse *messageResponse = [[MessageResponse alloc]init];
    NSError *error;
    NSString *jsonModified = [jsonString stringByReplacingOccurrencesOfString:@"<html><head></head><body>" withString:@""];
    jsonModified = [jsonModified stringByReplacingOccurrencesOfString:@"</body></html>" withString:@""];
    NSData *jsonData = [jsonModified dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary* json =[NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    NSLog(@"JSON:%@", jsonModified);
    
    NSDictionary* dicBooking = [json objectForKey:@"message"];
    
    NSLog(@"message status:%@", [dicBooking objectForKey:@"status"]);
    
    messageResponse = [[MessageResponse alloc]init];
    
    [messageResponse setStatus:[dicBooking valueForKey:@"status"]];
    [messageResponse setMessage:[dicBooking valueForKey:@"msg"]];
    
    return messageResponse;
}

- (NSString*) getRefNumberInItineraries:(NSArray*)itineraries
{
    NSString *refNumbers = @"";
    NSString *refNumber = @"";
    int count = 0;
    
    for (int i=0; i<itineraries.count; i++) {
        refNumber = ((BookingItinerary*)[itineraries objectAtIndex:i]).refNumber;
        //refNumbers = [refNumbers stringByAppendingString:refNumber];
        NSLog(@"itineraries.count:%d",itineraries.count);
        NSLog(@"refNumber:%@",refNumber);
        if ([refNumbers isEqualToString:@""]) {
            refNumbers =refNumber;
        }
        else
        {
            refNumbers = [NSString stringWithFormat:@"%@,%@",refNumbers,refNumber];
        }
        
        NSLog(@"refNumbers:%@",refNumbers);

    }
    
    return refNumbers;
}

- (BOOL) isAllSegmentConfirmedInItineraries:(NSArray*)itineraries
{
    NSString* status = @"";
    BOOL isConfirmed = YES;
    
    for (int i=0; i<itineraries.count; i++) {
        status = ((BookingItinerary*)[itineraries objectAtIndex:i]).status;
        //refNumbers = [refNumbers stringByAppendingString:refNumber];
        NSLog(@"itineraries.count:%d",itineraries.count);
        //NSLog(@"refNumber:%@",refNumber);
        if ([[status substringToIndex:2] isEqualToString:@"KL"] || [[status substringToIndex:2] isEqualToString:@"KK"]) {
            status = NO;
            break;
        }
        
        
        NSLog(@"##Status:%@",status);
        
    }
    
    return isConfirmed;
}
@end
