//
//  ServicingTableViewController.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 4/27/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PastBookingCell.h"
#import "BookingDisplayTableViewController.h"
#import "Booking.h"
#import "SVProgressHUD.h"
#import "ServicingBL.h"
#import "GLobal.h"
#import "StaffModel.h"
#import "PnrData.h"
#import "PnrDatadb.h"
#import "Utility.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"


#define SERVICING_INPUT @"Servicing Input"
#define BOOKINGREFERENCE_LIMIT 13


@interface ServicingTableViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,UIWebViewDelegate>
{
    Booking* booking;
    BookingItinerary* itn;
    NSMutableArray *pnrs;
    NSMutableArray *pnrsSwipe;
    PnrData *pnr;
}

@property (strong, nonatomic) Booking *booking;
@property (strong, nonatomic) IBOutlet UITableViewCell *bookingRefCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *displayBookingRefCell;
@property(nonatomic,strong) IBOutlet UIWebView *webView;
@property(nonatomic,strong) IBOutlet UITextField *bookingRefText;
- (void)UIWebViewWithPost:(UIWebView *)uiWebView url:(NSString *)url params:(NSMutableArray *)params;
- (IBAction)displayBooking:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *displayBookingOutletButton;
@property (strong, nonatomic) IBOutlet UITableView *servicingTableView;

@end
