//
//  SavingPnrViewController.h
//  StaffTravel
//
//  Created by Ali Mansho on 6/22/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PnrData.h"
#import "PnrDatadb.h"
#import "LoginUIViewController.h"

@interface SavingPnrViewController : UIViewController <UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UITextField *pnrTextField;
@property (strong, nonatomic) IBOutlet UITextField *dateTextField;
@property (strong, nonatomic) IBOutlet UITextField *originTextField;
@property (strong, nonatomic) IBOutlet UITextField *destinationTextField;
@property (strong, nonatomic) IBOutlet UITextField *jsonTextField;
@property (strong, nonatomic) IBOutlet UITextField *searchPnrTextField;
@property (strong, nonatomic) IBOutlet UITextField *prnTextField;
- (IBAction)savePnrDataButton:(id)sender;

- (IBAction)deletePnrDataButton:(id)sender;
- (IBAction)retrievePnrDataButton:(id)sender;

- (IBAction)updatePnrDataButton:(id)sender;
- (IBAction)logoutButton:(id)sender;
- (IBAction)clearFields:(id)sender;


@end
