//
//  LoginBL.m
//  StaffTravel
//
//  Created by Ali Mansho on 9/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "LoginBL.h"

@implementation LoginBL


//Write to the plist "UserInfoPlist" the variables [userID, password, prn, userName, ]
- (void) writeToPlist:(NSString *) userID userPassword:(NSString *) password payrollNumber:(NSString *)prn staffName:(NSString *) userName userGroups:(NSString *) groups error:(NSString *) errorMessage
{
    NSMutableDictionary* plistDic;
    BOOL doesExist;
    NSError *error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"UserInfoPlist" ofType:@"plist"]; //UserInfoPlist is the name of the plist file... //plistPath path in document directory;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * path =[[NSString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"UserInfoPlist.plist"]];
    
    doesExist= [fileManager fileExistsAtPath:path];
    if (doesExist) {
        plistDic=[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    }
    else
    {
        doesExist= [fileManager copyItemAtPath:filePath  toPath:path error:&error];
        plistDic=[[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    }
    
    [plistDic setObject:userID forKey:@"userID"];
    [plistDic setObject:password forKey:@"password"];
    [plistDic setObject:prn forKey:@"prn"];
    [plistDic setObject:userName forKey:@"userName"];
    //    [plistDic setObject:groups forKey:@"userGroups"];
    [plistDic setObject:errorMessage forKey:@"error"];
    
    [plistDic writeToFile:path atomically:YES];
    NSLog(@"Mansho - Utility - After editing the record - user info which is updated is: %@ ", plistDic);
}


//Read from the plist "UserInfoPlist" and return the variables []
+ (NSMutableDictionary *) readFromPlist
{
    NSMutableDictionary* plistDic;
    BOOL doesExist;
    NSError *error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"UserInfoPlist" ofType:@"plist"]; //UserInfoPlist is the name of the plist file... //plistPath path in document directory;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * path =[[NSString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"UserInfoPlist.plist"]];
    
    doesExist= [fileManager fileExistsAtPath:path];
    if (doesExist) {
        plistDic=[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    }
    else
    {
        doesExist= [fileManager copyItemAtPath:filePath  toPath:path error:&error];
        plistDic=[[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    }
    return plistDic;
}

//Read from the plist "UserInfoPlist" and return the variables []
+ (void) removeDataFromPlist
{
    NSMutableDictionary* plistDic;
    BOOL doesExist;
    NSError *error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"UserInfoPlist" ofType:@"plist"]; //UserInfoPlist is the name of the plist file... //plistPath path in document directory;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * path =[[NSString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"UserInfoPlist.plist"]];
    
    doesExist= [fileManager fileExistsAtPath:path];
    if (doesExist) {
        plistDic=[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    }
    else
    {
        doesExist= [fileManager copyItemAtPath:filePath  toPath:path error:&error];
        plistDic=[[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    }
    
    [plistDic removeObjectForKey:@"userID"];
    [plistDic removeObjectForKey:@"password"];
    [plistDic removeObjectForKey:@"prn"];
    [plistDic removeObjectForKey:@"userName"];
    //    [plistDic removeObjectForKey:@"userGroups"];
    [plistDic removeObjectForKey:@"error"];
    
    [plistDic writeToFile:path atomically:YES];
    NSLog(@"Mansho - Utility - After editing the record - user info which is updated is: %@ ", plistDic);
}


@end
