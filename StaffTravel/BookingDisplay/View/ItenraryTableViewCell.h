//
//  ItenraryTableViewCell.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 5/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItenraryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *flightNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *fromLabel;
@property (strong, nonatomic) IBOutlet UILabel *toLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *departureTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *arrivalTimeLabel;
@property(strong,nonatomic) IBOutlet UILabel * flightStatusLabel;
@end
