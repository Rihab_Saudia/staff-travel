//
//  PassengerTableViewCell.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 5/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PassengerTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *passengerName;

@end
