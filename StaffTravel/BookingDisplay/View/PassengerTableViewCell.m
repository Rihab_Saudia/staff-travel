//
//  PassengerTableViewCell.m
//  StaffTravel
//
//  Created by Mohammad Bahadur on 5/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "PassengerTableViewCell.h"

@implementation PassengerTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
