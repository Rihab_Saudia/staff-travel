//
//  BookingItinerary.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 6/16/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingItinerary : NSObject
@property (copy,nonatomic) NSString *carrier;
@property (copy,nonatomic) NSString *refNumber;
@property (copy,nonatomic) NSString *flightNo;
@property (copy,nonatomic) NSString *cabinClass;
@property (copy,nonatomic) NSString *day;
@property (copy,nonatomic) NSString *depDate; //Flight Date
@property (copy,nonatomic) NSString *origin;
@property (copy,nonatomic) NSString *dest;
@property (copy,nonatomic) NSString *departureTime;
@property (copy,nonatomic) NSString *arrivalTime;
@property (copy,nonatomic) NSString *status;

@end
