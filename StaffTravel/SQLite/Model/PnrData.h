//
//  PnrData.h
//  StaffTravel
//
//  Created by Ali Mansho on 6/12/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PnrData : NSObject

// the follwoing references have to be the same name as the ones in Data Base.
@property (strong, nonatomic) NSString *pnrC;
@property (nonatomic) NSString *flight_dateC;
@property (strong, nonatomic) NSString *originC;
@property (strong, nonatomic) NSString *destinationC;
@property (strong, nonatomic) NSString *jsonC;
@property (strong, nonatomic) NSString *prnC;
@end
