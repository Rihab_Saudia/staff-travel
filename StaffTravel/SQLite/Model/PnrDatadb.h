//
//  PnrDatadb.h
//  StaffTravel
//
//  Created by Ali Mansho on 6/12/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "SQLiteDatabase.h"
#import "PnrData.h"

@interface PnrDatadb : SQLiteDatabase
{
    sqlite3_stmt *insertPnr;
    sqlite3_stmt *updatePnr;
    sqlite3_stmt *deletePnr;
    sqlite3_stmt *selectPnr;
    sqlite3_stmt *selectPnrWithPnr;
    sqlite3_stmt *selectPnrWithPrn;
}

-(BOOL) insertIntoPnr: (NSString *) pnrC prn:(NSString *)prnC flightDate:(NSString *) flight_dateC origin:(NSString *) originC  destination:(NSString *) destinationC json:(NSString *) jsonC;

-(BOOL) updatePnr:(NSString *) pnrC prn:(NSString *) prnC flightDate:(NSString *) flight_dateC origin:(NSString *) originC destination:(NSString *) destinationC json:(NSString *) jsonC;

-(BOOL) deletePnr:(NSString *) pnrC;

-(NSArray *) selectFromPnr;
-(NSArray *) selectFromPnrDataWithPnr:(NSString *) pnrC;
-(NSMutableArray *) selectFromPnrDataWithPrn:(NSString *) prnC;

+(PnrDatadb *) sharedInstance; //This method is only to deal with locking the sql while updating records.

@end
