//
//  AppDelegate.m
//  StaffTravel
//
//  Created by Mohammed Alquzi on 4/22/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "HockeySDK/HockeySDK.h"
@implementation NSURLRequest (NSURLRequestWithIgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    
    return YES;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"a9daed4e6b047f80655417bf0e5fda02"];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    [[BITHockeyManager sharedHockeyManager].updateManager checkForUpdate];//To inform the user in case there is an update....

                              
                              NSLog(@"After calling the checkForUpdate Function...");
                              
                              NSMutableDictionary *plistDic = [LoginBL readFromPlist];
                              self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                              // Override point for customization after application launch.
                              self.window.backgroundColor = [UIColor whiteColor];
                              UINavigationController *navCon = [[ UINavigationController alloc] init];
                              GLobal *global = [GLobal sharedManager];
                              
                              // 1
                              [GAI sharedInstance].trackUncaughtExceptions = YES;
                              
                              // 2
                              [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
                              
                              // 3
                              [GAI sharedInstance].dispatchInterval = 20;
                              
                              // 4
                              //id <GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-54573002-1"];
                              
                              //    [self.window addSubview:.view];
                              //    [self.window makeKeyAndVisible];
                              //    [self.window addSubview:NSNav;
                              //    [self.window makeKeyAndVisible];
                              
                              
                              //    //1
                              //    UINavigationController *navCon1 = [[UINavigationController alloc] init];
                              //    //2
                              //    LoginUIViewController *FirstVC = [[LoginUIViewController alloc] init];
                              //    //3
                              //    [navCon1 setViewControllers:[NSArray arrayWithObjects: FirstVC]];
                              //    //4
                              //    [self.window setRootViewController:navCon1];
                              //
                              //    [self.window makeKeyAndVisible];
                              
                              //*******************
                              //Reading content of .plist file .. By Ali Mansho
                              //    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"UserInfoPlist" ofType:@"plist"]; //UserInfoPlist is the name of the plist file... //plistPath path in document directory;
                              //    NSMutableDictionary *plistDic = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
                              //
                              //    NSLog(@"Path in Delegate is: %@", plistPath);
                              //    NSLog(@"Delegate - User Information is saved as follow: %@", plistDic);
                              //
                              //    //Read values from the plist by Ali Mansho (A.M.)...
                              //    NSString *uname = [plistDic objectForKey:@"userID"];
                              //    NSString *pass = [plistDic objectForKey:@"password"];
                              //    NSLog(@"Mansho - user name which is saved is: %@ & password is: %@", uname, pass);
                              //
                              
                              
                              //Move the user to login screen to access his/her account in case no username and password are found in the plist. Otherwise, move the user to main menu screen directly by Ali Mansho...
                              
                              if ([[plistDic objectForKey:@"prn"] length] == 0)
                              {
                                  self.loginMainFlag = YES; //It means that the user is comeing from LoginScreen
                                  // NSLog(@"loginMain Flag: %hhd", self.loginMainFlag);
                                  LoginUIViewController *FstVC = [[LoginUIViewController alloc] init];
                                  navCon = [[UINavigationController alloc] initWithRootViewController:FstVC]; //7
                                  [navCon setViewControllers:[NSArray arrayWithObject:FstVC]];
                                  self.window.rootViewController = navCon; //8
                                  [self.window makeKeyAndVisible];
                              }
                              
                              else
                              {
                                  self.loginMainFlag = NO;//It means that the user is comeing from MainMenu
                                  
                                  //         NSLog(@"loginMain Flag: %hhd",  self.loginMainFlag);
                                  //        NSLog(@"Mansho - Length is larger than Zero");
                                  
                                  //We need here to feed the Staff Model to display Staff name and get the Payroll number from the plist. The plist has the parameters: userID, password, userName, groups, error, and prn.
                                  
                                  self.staff        = [[StaffModel alloc]init];
                                  self.staff.prn    = [plistDic objectForKey:@"prn"];
                                  self.staff.name   = [plistDic objectForKey:@"userName"];
                                  self.staff.error  = [plistDic objectForKey:@"error"];
                                  self.staff.groups = [plistDic objectForKey:@"userGroups"];
                                  //The above objectForKey values are saved in the plist...
                                  
                                  MainMenuViewController *FstVC = [[MainMenuViewController alloc] init];
                                  FstVC.staff = self.staff;
                                  global.staff = self.staff;
                                  navCon = [[UINavigationController alloc] initWithRootViewController:FstVC]; //7
                                  [navCon setViewControllers:[NSArray arrayWithObject:FstVC]];
                                  self.window.rootViewController = navCon; //8
                                  [self.window makeKeyAndVisible];
                                  // MainMenuViewController *mainView = [[MainMenuViewController alloc] init];
                                  //self.window.rootViewController = mainView;
                                  //[self.window setRootViewController:mainView];
                              }
                              //******************************
                              
                              //    UINavigationController *navCon = [[ UINavigationController alloc] init];
                              //    MainMenuViewController *FstVC = [[MainMenuViewController alloc] init];
                              //    //navCon =[[LoginUIViewController alloc] init]; //6
                              //    navCon = [[UINavigationController alloc] initWithNibName:(@"MainMenuViewController") bundle:FstVC]; //7
                              //    [navCon setViewControllers:[NSArray arrayWithObject:FstVC]];
                              //    //8
                              //    [self.window makeKeyAndVisible];
                              
                              return YES;
                              }
                              
    /*- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
     {
     if ( event.subtype == UIEventSubtypeMotionShake )
     {
     // Put in code here to handle shake
     NSLog(@"Device Shaked");
     
     
     }
     
     if ( [super respondsToSelector:@selector(motionEnded:withEvent:)] )
     [super motionEnded:motion withEvent:event];
     }
     
     - (BOOL)canBecomeFirstResponder
     {
     return YES;
     }
     */
                              
                              - (void)applicationWillResignActive:(UIApplication *)application
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
                              
                              - (void)applicationDidEnterBackground:(UIApplication *)application
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
                              
                              - (void)applicationWillEnterForeground:(UIApplication *)application
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
                              
                              - (void)applicationDidBecomeActive:(UIApplication *)application
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
        //    Reachability *reachability = [Reachability reachabilityForInternetConnection];
        //
        //    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        //
        //    if(internetStatus == NotReachable) {
        //        UIAlertView *errorView;
        //
        //        errorView = [[UIAlertView alloc]
        //                     initWithTitle: NSLocalizedString(@"No internet connection found.", @"Network error")
        //                     message: NSLocalizedString(@"You must connect to a Wi-Fi or cellular data network to access Saudia Staff Travel.", @"Network error")
        //                     delegate: self
        //                     cancelButtonTitle: NSLocalizedString(@"Close", @"Network error") otherButtonTitles: nil];
        //
        //        [errorView show];
        //        //  [errorView autorelease];
        //    }
        //
        
        
    }
                              
    - (void)applicationWillTerminate:(UIApplication *)application
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
                              
                              @end
