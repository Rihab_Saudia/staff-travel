//
//  ContactUsViewController.m
//  StaffTravel
//
//  Created by Mohammed Alquzi on 6/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
///

#import "ContactUsViewController.h"

@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:CONTACT_US];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Page title
    self.title = @"Contact us";

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
}
- (IBAction)CallFlightBookingDept:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://+966920022222"]];
}

- (IBAction)CallHelpDesk:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://+966126864000"]];
}

- (IBAction)EmailMobileAppTeamButton:(id)sender {
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	
    if(mailClass != nil)
    {
        if ([mailClass canSendMail])
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            NSArray *toRecipients = [NSArray arrayWithObjects:@"mobiledev@saudiairlines.com", nil];
            
            [picker setSubject:@"[iOS] - Report an issue to Saudia Mobile Applicatoin Team"];
            
            [picker setToRecipients:toRecipients];
            
            
            
            //            NSString *eMailBody = @"<a href='http://www.softkraftsolutions.com/nimap/brainiack.html'><img src='http://www.iphonedevelopmentguide.com/images/brainiack.jpg' alt='Brainiack' width='128' height='128'/></a><br /><br />Hey guys just played Brainiack. Its really addictive game. You too can grab your copy now <a href='http://www.softkraftsolutions.com/nimap/brainiack.html'>click here</a>!!!";
            
            NSString *eMailBody = @"";
            
            [picker setMessageBody:eMailBody isHTML:YES];
            
            [self presentViewController:picker animated:YES completion:nil];
            // [picker release];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Cannot send email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Feature not available." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert1 show];
    }
	
}


-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	// Notifies users about errors associated with the interface
    NSString *message;
    
	switch (result)
	{
		case MFMailComposeResultCancelled:
			message = @"Email Canceled";
			break;
		case MFMailComposeResultSaved:
			message = @"Email Saved";
			break;
		case MFMailComposeResultSent:
			message = @"Thank you for emailing us. We will get back to you as soon as possible.";
			break;
		case MFMailComposeResultFailed:
			message = @"Email Failed";
			break;
		default:
			message = @"Email Not Sent";
			break;
	}
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
    
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
    {
        //post-iOS6.0
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        // pre-iOS6.0
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [self dismissModalViewControllerAnimated:YES];
#pragma clang diagnostic pop
    }
	//[self resignFirstResponder];
}


@end
